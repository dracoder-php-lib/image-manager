<?php

namespace Dracoder\ImageManager;

use Dracoder\Bundle\FileManagerBundle\UploaderManager;
use Dracoder\Bundle\FileManagerBundle\UploadStatus;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class ImageUploader extends UploaderManager
{
    /** @var string[] $allowedExtensions */
    protected array $allowedExtensions = ['jpeg', 'png', 'jpg', 'gif', 'otf'];

    /** @var int $maxWidth */
    private int $maxWidth;

    /** @var int $maxHeight */
    private int $maxHeight;

    public function __construct(
        $projectDir,
        array $allowedExtensions = ['jpeg', 'png', 'jpg', 'gif', 'otf'],
        string $folder = '.',
        int $maxHeight = 3112,
        int $maxWidth = 4096
    ) {
        parent::__construct($projectDir, $folder, $allowedExtensions);

        $this->maxHeight = $maxHeight;
        $this->maxWidth = $maxWidth;
    }

    /**
     *
     * @return int
     */
    public function getMaxWidth(): int
    {
        return $this->maxWidth;
    }

    /**
     *
     * @param $width
     *
     * @return ImageUploader
     */
    public function setMaxWidth(int $width): self
    {
        $this->maxWidth = $width;

        return $this;
    }

    /**
     *
     * @return int
     */
    public function getMaxHeight(): int
    {
        return $this->maxHeight;
    }

    /**
     *
     * @param int $height
     *
     * @return ImageUploader
     */
    public function setMaxHeight(int $height): self
    {
        $this->maxHeight = $height;

        return $this;
    }

    /**
     *
     * @param UploadedFile $file
     *
     * @param string|null $exceptionHandlerClass
     *
     * @return bool|string
     */
    public function upload(UploadedFile $file, string $exceptionHandlerClass = null)
    {
        $extension = $file->guessExtension();
        if (!in_array($extension, $this->allowedExtensions, true)) {
            $this->state = new UploadStatus(UploadStatus::INVALID_FORMAT);

            return false;
        }
        if (!$this->allowedDimension($file)) {
            $this->state = new UploadStatus(UploadStatus::INVALID_DIMENSIONS);

            return false;
        }

        return parent::upload($file);
    }

    /**
     *
     * @param UploadedFile $image
     *
     * @return bool
     */
    private function allowedDimension(UploadedFile $image): bool
    {
        $dimensions = getimagesize($image);
        if ($this->maxWidth > 0 && $dimensions[0] > $this->maxWidth) {
            return false;
        }

        if ($this->maxHeight > 0 && $dimensions[1] > $this->maxHeight) {
            return false;
        }

        return true;
    }
}