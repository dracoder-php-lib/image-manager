<?php
/**
 * Created by PhpStorm.
 *
 * @author Gonzalo Sáenz
 * Date: 10/01/2019
 * Time: 19:26
 */


namespace Dracoder\ImageManager;


class ImageDimension
{
    public const HEIGHT = 'h';
    public const WIDTH = 'w';
}